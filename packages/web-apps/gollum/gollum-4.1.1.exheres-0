# Copyright 2016 Kylie McClain <somasis@exhebo.org>
# Distributed under the terms of the GNU General Public License v2

require gem [ has_rdoc=true ]

SUMMARY="A simple, Git-powered wiki with a sweet API and local frontend"
LICENCES="MIT"

SLOT="0"
PLATFORMS="~amd64"

MYOPTIONS=""
DEPENDENCIES="
    build+run:
        dev-ruby/gollum-lib[>=$(ever range 1-2)][ruby_abis:*(-)?]
        dev-ruby/gemojione[~>3.2&][ruby_abis:*(-)?]
        dev-ruby/sinatra[~>1.4&>=1.4.4][ruby_abis:*(-)?]
        dev-ruby/mustache:0[>=0.99.5&<1.0.0][ruby_abis:*(-)?]
        dev-ruby/useragent[~>0.16.2][ruby_abis:*(-)?]
        dev-ruby/kramdown[~>1.9.0][ruby_abis:*(-)?]
    recommendation:
        dev-ruby/gollum-rugged_adapter[ruby_abis:*(-)?] [[
            description = [ rugged_adapter works a lot better than grit and is more maintained. ]
        ]]
    suggestion:
        app-text/asciidoctor[ruby_abis:*(-)?] [[
            description = [ Gollum can use it for rendering AsciiDoc ]
            group-name = [ asciidoc ]
        ]]
        dev-ruby/creole[ruby_abis:*(-)?] [[
            description = [ Gollum can use it for rendering Creole ]
            group-name = [ creole ]
        ]]
        dev-ruby/wikicloth[ruby_abis:*(-)?] [[
            description = [ Gollum can use it for rendering MediaWiki ]
            group-name = [ mediawiki ]
        ]]
        dev-ruby/org-ruby[ruby_abis:*(-)?] [[
            description = [ Gollum can use it for rendering Org ]
            group-name = [ org ]
        ]]
        dev-lang/perl[>=5.10] [[
            description = [ Gollum can use Perl to render Pod ]
            group-name = [ pod ]
        ]]
        (
            dev-lang/python[>=2&<3]
            dev-python/docutils[python_abis:2.7]
        ) [[
            description = [ Gollum can use python2 and docutils to render ReStructuredText ]
            group-name = [ restructuredtext ]
        ]]
        dev-ruby/RedCloth[ruby_abis:*(-)?] [[
            description = [ Gollum can use it to render Textile ]
            group-name = [ textile ]
        ]]
"

