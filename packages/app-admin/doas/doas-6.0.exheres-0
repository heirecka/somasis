# Copyright 2016 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user='Duncaen' pn='OpenDoas' tag="v${PV}" ]

SUMMARY="A portable version of the OpenBSD \`doas\` command"
SLOT="0"
LICENCES="ISC"

MYOPTIONS="debug"

PLATFORMS="~amd64"

DEPENDENCIES="
    build+run:
        sys-libs/pam
"

src_prepare() {
    default
    edo sed -e 's/-Werror//' -i Makefile
}

src_configure() {
    export VERSION="${PV}"

    edo ./configure \
        --build=$(exhost --build) --host=$(exhost --target) --target=$(exhost --target) \
        --prefix=/usr --exec-prefix=/usr/$(exhost --target) --bindir=/usr/$(exhost --target)/bin \
        --with-pam $(optionq debug && echo --enable-debug)

}

src_install() {
    default

    insinto /etc
    insopts -m600
    hereins doas.conf <<EOF
# See doas.conf(5) for information on configuring doas

# Permit users in the wheel group to elevate to root
permit :wheel
EOF
}
