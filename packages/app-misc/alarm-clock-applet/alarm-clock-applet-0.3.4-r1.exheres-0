# Copyright 2015-2016 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require launchpad [ pn=${PN//-applet} branch=trunk pv=${PV} pnv=${PN}-${PV} ]
require gconf gtk-icon-cache

SUMMARY="A fully-featured alarm clock for your GNOME panel or equivalent"

SLOT="0"
LICENCES="GPL-2"
PLATFORMS="~amd64"

MYOPTIONS="
    appindicator [[ description = [ Use AppIndicator for tray icon rather than a standard tray icon ] ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
    build+run:
        dev-libs/glib:2[>=2.16.0]
        dev-libs/libxml2
        dev-libs/unique:1
        gnome-desktop/adwaita-icon-theme
        gnome-platform/GConf:2
        media-libs/gstreamer:0.10
        x11-libs/gtk+:2[>=2.12.0]
        x11-libs/libnotify[>=0.4.1]
        appindicator? ( dev-libs/libappindicator:0.1[gtk2] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-run-in-source-tree
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    appindicator
)

# tests fail due to compilation error?
RESTRICT="test"

src_prepare() {
    edo intltoolize --force --copy
    autotools_src_prepare
}

pkg_preinst() {
    gtk-icon-cache_pkg_preinst
    gconf_pkg_preinst
}

pkg_postinst() {
    gtk-icon-cache_pkg_postinst
    gconf_pkg_postinst
}

pkg_postrm() {
    gtk-icon-cache_pkg_postrm
    gconf_pkg_postrm
}
