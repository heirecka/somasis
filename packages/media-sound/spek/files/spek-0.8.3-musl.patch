Upstream: yes, backported from git master

diff --git a/src/spek-audio.cc b/src/spek-audio.cc
index ab70fdc..3fbd12d 100644
--- a/src/spek-audio.cc
+++ b/src/spek-audio.cc
@@ -31,7 +31,7 @@ public:
     AudioFileImpl(
         AudioError error, AVFormatContext *format_context, int audio_stream,
         const std::string& codec_name, int bit_rate, int sample_rate, int bits_per_sample,
-        int channels, double duration, bool is_planar, int width, bool fp
+        int channels, double duration
     );
     ~AudioFileImpl() override;
     void start(int samples) override;
@@ -44,9 +44,7 @@ public:
     int get_bits_per_sample() const override { return this->bits_per_sample; }
     int get_channels() const override { return this->channels; }
     double get_duration() const override { return this->duration; }
-    int get_width() const override { return this->width; }
-    bool get_fp() const override { return this->fp; }
-    const uint8_t *get_buffer() const override { return this->buffer; }
+    const float *get_buffer() const override { return this->buffer; }
     int64_t get_frames_per_interval() const override { return this->frames_per_interval; }
     int64_t get_error_per_interval() const override { return this->error_per_interval; }
     int64_t get_error_base() const override { return this->error_base; }
@@ -61,15 +59,12 @@ private:
     int bits_per_sample;
     int channels;
     double duration;
-    bool is_planar;
-    int width;
-    bool fp;
 
     AVPacket packet;
     int offset;
     AVFrame *frame;
-    int buffer_size;
-    uint8_t *buffer;
+    int buffer_len;
+    float *buffer;
     // TODO: these guys don't belong here, move them somewhere else when revamping the pipeline
     int64_t frames_per_interval;
     int64_t error_per_interval;
@@ -163,20 +158,12 @@ std::unique_ptr<AudioFile> Audio::open(const std::string& file_name)
         error = AudioError::CANNOT_OPEN_DECODER;
     }
 
-    bool is_planar = false;
-    int width = 0;
-    bool fp = false;
     if (!error) {
-        is_planar = av_sample_fmt_is_planar(codec_context->sample_fmt);
-        width = av_get_bytes_per_sample(codec_context->sample_fmt);
         AVSampleFormat fmt = codec_context->sample_fmt;
-        if (fmt == AV_SAMPLE_FMT_S16 || fmt == AV_SAMPLE_FMT_S16P ||
-            fmt == AV_SAMPLE_FMT_S32 || fmt == AV_SAMPLE_FMT_S32P) {
-            fp = false;
-        } else if (fmt == AV_SAMPLE_FMT_FLT || fmt == AV_SAMPLE_FMT_FLTP ||
-            fmt == AV_SAMPLE_FMT_DBL || fmt == AV_SAMPLE_FMT_DBLP ) {
-            fp = true;
-        } else {
+        if (fmt != AV_SAMPLE_FMT_S16 && fmt != AV_SAMPLE_FMT_S16P &&
+            fmt != AV_SAMPLE_FMT_S32 && fmt != AV_SAMPLE_FMT_S32P &&
+            fmt != AV_SAMPLE_FMT_FLT && fmt != AV_SAMPLE_FMT_FLTP &&
+            fmt != AV_SAMPLE_FMT_DBL && fmt != AV_SAMPLE_FMT_DBLP ) {
             error = AudioError::BAD_SAMPLE_FORMAT;
         }
     }
@@ -184,26 +171,26 @@ std::unique_ptr<AudioFile> Audio::open(const std::string& file_name)
     return std::unique_ptr<AudioFile>(new AudioFileImpl(
         error, format_context, audio_stream,
         codec_name, bit_rate, sample_rate, bits_per_sample,
-        channels, duration, is_planar, width, fp
+        channels, duration
     ));
 }
 
 AudioFileImpl::AudioFileImpl(
     AudioError error, AVFormatContext *format_context, int audio_stream,
     const std::string& codec_name, int bit_rate, int sample_rate, int bits_per_sample,
-    int channels, double duration, bool is_planar, int width, bool fp
+    int channels, double duration
 ) :
     error(error), format_context(format_context), audio_stream(audio_stream),
     codec_name(codec_name), bit_rate(bit_rate),
     sample_rate(sample_rate), bits_per_sample(bits_per_sample),
-    channels(channels), duration(duration), is_planar(is_planar), width(width), fp(fp)
+    channels(channels), duration(duration)
 {
     av_init_packet(&this->packet);
     this->packet.data = nullptr;
     this->packet.size = 0;
     this->offset = 0;
     this->frame = avcodec_alloc_frame();
-    this->buffer_size = 0;
+    this->buffer_len = 0;
     this->buffer = nullptr;
     this->frames_per_interval = 0;
     this->error_per_interval = 0;
@@ -276,27 +263,56 @@ int AudioFileImpl::read()
             // We have data, return it and come back for more later.
             int samples = this->frame->nb_samples;
             int channels = this->channels;
-            int width = this->width;
-            int buffer_size = samples * channels * width;
-            if (buffer_size > this->buffer_size) {
-                this->buffer = (uint8_t*)av_realloc(this->buffer, buffer_size);
-                this->buffer_size = buffer_size;
+            int buffer_len = samples * channels;
+            if (buffer_len > this->buffer_len) {
+                this->buffer = static_cast<float*>(
+                    av_realloc(this->buffer, buffer_len * sizeof(float))
+                );
+                this->buffer_len = buffer_len;
             }
-            if (this->is_planar) {
+
+            AVSampleFormat format = static_cast<AVSampleFormat>(this->frame->format);
+            int is_planar = av_sample_fmt_is_planar(format);
+            int i = 0;
+            for (int sample = 0; sample < samples; ++sample) {
                 for (int channel = 0; channel < channels; ++channel) {
-                    uint8_t *buffer = this->buffer + channel * width;
-                    uint8_t *data = this->frame->data[channel];
-                    for (int sample = 0; sample < samples; ++sample) {
-                        for (int i = 0; i < width; ++i) {
-                            *buffer++ = *data++;
-                        }
-                        buffer += (channels - 1) * width;
+                    uint8_t *data;
+                    int offset;
+                    if (is_planar) {
+                        data = this->frame->data[channel];
+                        offset = sample;
+                    } else {
+                        data = this->frame->data[0];
+                        offset = i;
+                    }
+                    float value;
+                    switch (format) {
+                    case AV_SAMPLE_FMT_S16:
+                    case AV_SAMPLE_FMT_S16P:
+                        value = reinterpret_cast<int16_t*>(data)[offset]
+                            / static_cast<float>(INT16_MAX);
+                        break;
+                    case AV_SAMPLE_FMT_S32:
+                    case AV_SAMPLE_FMT_S32P:
+                        value = reinterpret_cast<int32_t*>(data)[offset]
+                            / static_cast<float>(INT32_MAX);
+                        break;
+                    case AV_SAMPLE_FMT_FLT:
+                    case AV_SAMPLE_FMT_FLTP:
+                        value = reinterpret_cast<float*>(data)[offset];
+                        break;
+                    case AV_SAMPLE_FMT_DBL:
+                    case AV_SAMPLE_FMT_DBLP:
+                        value = reinterpret_cast<double*>(data)[offset];
+                        break;
+                    default:
+                        value = 0.0f;
+                        break;
                     }
+                    this->buffer[i++] = value;
                 }
-            } else {
-                memcpy(this->buffer, this->frame->data[0], buffer_size);
             }
-            return buffer_size;
+            return buffer_len;
         }
         if (this->packet.data) {
             this->packet.data -= this->offset;
diff --git a/src/spek-audio.h b/src/spek-audio.h
index c145811..579c3bf 100644
--- a/src/spek-audio.h
+++ b/src/spek-audio.h
@@ -49,9 +49,7 @@ public:
     virtual int get_bits_per_sample() const = 0;
     virtual int get_channels() const = 0;
     virtual double get_duration() const = 0;
-    virtual int get_width() const = 0;
-    virtual bool get_fp() const = 0;
-    virtual const uint8_t *get_buffer() const = 0;
+    virtual const float *get_buffer() const = 0;
     virtual int64_t get_frames_per_interval() const = 0;
     virtual int64_t get_error_per_interval() const = 0;
     virtual int64_t get_error_base() const = 0;
diff --git a/src/spek-pipeline.cc b/src/spek-pipeline.cc
index f5465bb..8ff8e4b 100644
--- a/src/spek-pipeline.cc
+++ b/src/spek-pipeline.cc
@@ -83,7 +83,6 @@ struct spek_pipeline
 static void * reader_func(void *);
 static void * worker_func(void *);
 static void reader_sync(struct spek_pipeline *p, int pos);
-static float average_input(const struct spek_pipeline *p, const void *buffer);
 
 struct spek_pipeline * spek_pipeline_open(
     std::unique_ptr<AudioFile> file, int bands, int samples, spek_pipeline_cb cb, void *cb_data)
@@ -295,16 +294,20 @@ static void * reader_func(void *pp)
     }
 
     int pos = 0, prev_pos = 0;
-    int block_size = p->file->get_width() * p->file->get_channels();
-    int size;
-    while ((size = p->file->read()) > 0) {
+    int channels = p->file->get_channels();
+    int len;
+    while ((len = p->file->read()) > 0) {
         if (p->quit) break;
 
-        const uint8_t *buffer = p->file->get_buffer();
-        while (size >= block_size) {
-            p->input[pos] = average_input(p, buffer);
-            buffer += block_size;
-            size -= block_size;
+        const float *buffer = p->file->get_buffer();
+        while (len >= channels) {
+            float val = 0.0f;
+            for (int i = 0; i < channels; i++) {
+                val += buffer[i];
+            }
+            p->input[pos] = val / channels;
+            buffer += channels;
+            len -= channels;
             pos = (pos + 1) % p->input_size;
 
             // Wake up the worker if we have enough data.
@@ -312,7 +315,7 @@ static void * reader_func(void *pp)
                 reader_sync(p, prev_pos = pos);
             }
         }
-        assert(size == 0);
+        assert(len == 0);
     }
 
     if (pos != prev_pos) {
@@ -431,37 +434,3 @@ static void * worker_func(void *pp)
         }
     }
 }
-
-static float average_input(const struct spek_pipeline *p, const void *buffer)
-{
-    int channels = p->file->get_channels();
-    float res = 0.0f;
-    if (p->file->get_fp()) {
-        if (p->file->get_width() == 4) {
-            float *b = (float*)buffer;
-            for (int i = 0; i < channels; i++) {
-                res += b[i];
-            }
-        } else {
-            assert(p->file->get_width() == 8);
-            double *b = (double*)buffer;
-            for (int i = 0; i < channels; i++) {
-                res += (float) b[i];
-            }
-        }
-    } else {
-        if (p->file->get_width() == 2) {
-            int16_t *b = (int16_t*)buffer;
-            for (int i = 0; i < channels; i++) {
-                res += b[i] / (float) INT16_MAX;
-            }
-        } else {
-            assert (p->file->get_width() == 4);
-            int32_t *b = (int32_t*)buffer;
-            for (int i = 0; i < channels; i++) {
-                res += b[i] / (float) INT32_MAX;
-            }
-        }
-    }
-    return res / channels;
-}
diff --git a/src/spek-pipeline.cc b/src/spek-pipeline.cc
index 8ff8e4b..cc194ce 100644
--- a/src/spek-pipeline.cc
+++ b/src/spek-pipeline.cc
@@ -23,7 +23,6 @@
  * (c) 2007-2009 Sebastian Dröge <sebastian.droege@collabora.co.uk>
  */
 
-#define __STDC_LIMIT_MACROS
 #include <wx/intl.h>
 
 #include <assert.h>
diff --git a/src/spek-audio.cc b/src/spek-audio.cc
index 3fbd12d..e519b9c 100644
--- a/src/spek-audio.cc
+++ b/src/spek-audio.cc
@@ -18,6 +18,7 @@
 
 extern "C" {
 #define __STDC_CONSTANT_MACROS
+#define __STDC_LIMIT_MACROS
 #include <libavformat/avformat.h>
 #include <libavcodec/avcodec.h>
 #include <libavutil/mathematics.h>
diff --git a/src/spek-preferences-dialog.cc b/src/spek-preferences-dialog.cc
index 5f1fb38..94dcb9e 100644
--- a/src/spek-preferences-dialog.cc
+++ b/src/spek-preferences-dialog.cc
@@ -40,6 +40,7 @@ static const char *available_languages[] =
     "pl", "Polski",
     "pt_BR", "Português brasileiro",
     "ru", "Русский",
+    "sr@latin", "Srpski",
     "sv", "Svenska",
     "tr", "Türkçe",
     "uk", "Українська",
diff --git a/src/spek-window.cc b/src/spek-window.cc
index a49953d..ee8e262 100644
--- a/src/spek-window.cc
+++ b/src/spek-window.cc
@@ -311,7 +311,7 @@ void SpekWindow::on_preferences(wxCommandEvent&)
 void SpekWindow::on_help(wxCommandEvent&)
 {
     wxLaunchDefaultBrowser(
-        wxString::Format(wxT("http://spek-project.org/man-%s.html"), wxT(PACKAGE_VERSION))
+        wxString::Format(wxT("http://spek.cc/man-%s.html"), wxT(PACKAGE_VERSION))
     );
 }
 
@@ -337,7 +337,7 @@ void SpekWindow::on_about(wxCommandEvent&)
     info.SetCopyright(_("Copyright (c) 2010-2013 Alexander Kojevnikov and contributors"));
     info.SetDescription(this->description);
 #ifdef OS_UNIX
-    info.SetWebSite(wxT("http://spek-project.org/"), _("Spek Website"));
+    info.SetWebSite(wxT("http://spek.cc/"), _("Spek Website"));
     info.SetIcon(wxArtProvider::GetIcon(wxT("spek"), wxART_OTHER, wxSize(128, 128)));
 #endif
     wxAboutBox(info);
@@ -351,7 +351,7 @@ void SpekWindow::on_notify(wxCommandEvent&)
 
 void SpekWindow::on_visit(wxCommandEvent&)
 {
-    wxLaunchDefaultBrowser(wxT("http://spek-project.org"));
+    wxLaunchDefaultBrowser(wxT("http://spek.cc"));
 }
 
 void SpekWindow::on_close(wxCommandEvent& event)
@@ -387,7 +387,7 @@ static void * check_version(void *p)
     // Get the version number.
     wxString version;
     wxHTTP http;
-    if (http.Connect(wxT("spek-project.org"))) {
+    if (http.Connect(wxT("spek.cc"))) {
         wxInputStream *stream = http.GetInputStream(wxT("/version"));
         if (stream) {
             wxStringOutputStream out(&version);
diff --git a/src/spek-audio.cc b/src/spek-audio.cc
index e519b9c..f1daebb 100644
--- a/src/spek-audio.cc
+++ b/src/spek-audio.cc
@@ -134,9 +134,12 @@ std::unique_ptr<AudioFile> Audio::open(const std::string& file_name)
         bits_per_sample = codec_context->bits_per_raw_sample;
         if (!bits_per_sample) {
             // APE uses bpcs, FLAC uses bprs.
-            // TODO: old comment, verify
             bits_per_sample = codec_context->bits_per_coded_sample;
         }
+        if (codec_context->codec_id == AV_CODEC_ID_AAC) {
+            // AAC decoder sets both bps and bitrate.
+            bits_per_sample = 0;
+        }
         if (bits_per_sample) {
             bit_rate = 0;
         }
diff --git a/src/spek-audio.cc b/src/spek-audio.cc
index f1daebb..fdde397 100644
--- a/src/spek-audio.cc
+++ b/src/spek-audio.cc
@@ -103,9 +103,9 @@ std::unique_ptr<AudioFile> Audio::open(const std::string& file_name)
                 break;
             }
         }
-    }
-    if (audio_stream == -1) {
-        error = AudioError::NO_AUDIO;
+        if (audio_stream == -1) {
+            error = AudioError::NO_AUDIO;
+        }
     }
 
     AVStream *stream = nullptr;
diff --git a/src/spek-audio.cc b/src/spek-audio.cc
index fdde397..5c1615f 100644
--- a/src/spek-audio.cc
+++ b/src/spek-audio.cc
@@ -136,8 +136,9 @@ std::unique_ptr<AudioFile> Audio::open(const std::string& file_name)
             // APE uses bpcs, FLAC uses bprs.
             bits_per_sample = codec_context->bits_per_coded_sample;
         }
-        if (codec_context->codec_id == AV_CODEC_ID_AAC) {
-            // AAC decoder sets both bps and bitrate.
+        if (codec_context->codec_id == AV_CODEC_ID_AAC ||
+            codec_context->codec_id == AV_CODEC_ID_MUSEPACK8) {
+            // These decoders set both bps and bitrate.
             bits_per_sample = 0;
         }
         if (bits_per_sample) {
diff --git a/src/spek-audio.cc b/src/spek-audio.cc
index 5c1615f..2c68165 100644
--- a/src/spek-audio.cc
+++ b/src/spek-audio.cc
@@ -137,7 +137,9 @@ std::unique_ptr<AudioFile> Audio::open(const std::string& file_name)
             bits_per_sample = codec_context->bits_per_coded_sample;
         }
         if (codec_context->codec_id == AV_CODEC_ID_AAC ||
-            codec_context->codec_id == AV_CODEC_ID_MUSEPACK8) {
+            codec_context->codec_id == AV_CODEC_ID_MUSEPACK8 ||
+            codec_context->codec_id == AV_CODEC_ID_WMAV1 ||
+            codec_context->codec_id == AV_CODEC_ID_WMAV2) {
             // These decoders set both bps and bitrate.
             bits_per_sample = 0;
         }
diff --git a/src/spek-preferences-dialog.cc b/src/spek-preferences-dialog.cc
index 94dcb9e..7417607 100644
--- a/src/spek-preferences-dialog.cc
+++ b/src/spek-preferences-dialog.cc
@@ -26,6 +26,7 @@
 static const char *available_languages[] =
 {
     "", "",
+    "ca", "Català",
     "cs", "Čeština",
     "da", "Dansk",
     "de", "Deutsch",
@@ -33,6 +34,7 @@ static const char *available_languages[] =
     "eo", "Esperanto",
     "es", "Español",
     "fr", "Français",
+    "gl", "Galego",
     "it", "Italiano",
     "ja", "日本語",
     "nb", "Norsk (bokmål)",
diff --git a/src/spek-fft.cc b/src/spek-fft.cc
index 7b4e8a9..c48f23a 100644
--- a/src/spek-fft.cc
+++ b/src/spek-fft.cc
@@ -1,6 +1,6 @@
 /* spek-fft.cc
  *
- * Copyright (C) 2010-2012  Alexander Kojevnikov <alexander@kojevnikov.com>
+ * Copyright (C) 2010-2013  Alexander Kojevnikov <alexander@kojevnikov.com>
  *
  * Spek is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
@@ -16,53 +16,53 @@
  * along with Spek.  If not, see <http://www.gnu.org/licenses/>.
  */
 
-#include <math.h>
+#include <cmath>
 
 #define __STDC_CONSTANT_MACROS
 extern "C" {
 #include <libavcodec/avfft.h>
-#include <libavutil/mem.h>
 }
 
 #include "spek-fft.h"
 
-struct spek_fft_plan * spek_fft_plan_new(int n)
+class FFTPlanImpl : public FFTPlan
 {
-    struct spek_fft_plan *p = (spek_fft_plan*)malloc(sizeof(struct spek_fft_plan));
-    p->input = (float*)av_mallocz(sizeof(float) * n);
-    p->output = (float*)av_mallocz(sizeof(float) * (n / 2 + 1));
-    int bits = 0;
-    while (n) {
-        n >>= 1;
-        ++bits;
-    }
-    p->n = 1 << --bits;
-    p->cx = av_rdft_init(bits, DFT_R2C);
-    return p;
+public:
+    FFTPlanImpl(int nbits);
+    ~FFTPlanImpl() override;
+
+    void execute() override;
+
+private:
+    struct RDFTContext *cx;
+};
+
+std::unique_ptr<FFTPlan> FFT::create(int nbits)
+{
+    return std::unique_ptr<FFTPlan>(new FFTPlanImpl(nbits));
+}
+
+FFTPlanImpl::FFTPlanImpl(int nbits) : FFTPlan(nbits), cx(av_rdft_init(nbits, DFT_R2C))
+{
+}
+
+FFTPlanImpl::~FFTPlanImpl()
+{
+    av_rdft_end(this->cx);
 }
 
-void spek_fft_execute(struct spek_fft_plan *p)
+void FFTPlanImpl::execute()
 {
-    av_rdft_calc(p->cx, p->input);
+    av_rdft_calc(this->cx, this->get_input());
 
     // Calculate magnitudes.
-    int n = p->n;
+    int n = this->get_input_size();
     float n2 = n * n;
-    p->output[0] = 10.0f * log10f(p->input[0] * p->input[0] / n2);
-    p->output[n / 2] = 10.0f * log10f(p->input[1] * p->input[1] / n2);
+    this->set_output(0, 10.0f * log10f(this->get_input(0) * this->get_input(0) / n2));
+    this->set_output(n / 2, 10.0f * log10f(this->get_input(1) * this->get_input(1) / n2));
     for (int i = 1; i < n / 2; i++) {
-        float val =
-            p->input[i * 2] * p->input[i * 2] +
-            p->input[i * 2 + 1] * p->input[i * 2 + 1];
-        val /= n2;
-        p->output[i] = 10.0f * log10f(val);
+        float re = this->get_input(i * 2);
+        float im = this->get_input(i * 2 + 1);
+        this->set_output(i, 10.0f * log10f((re * re + im * im) / n2));
     }
 }
-
-void spek_fft_delete(struct spek_fft_plan *p)
-{
-    av_rdft_end(p->cx);
-    av_free(p->input);
-    av_free(p->output);
-    free(p);
-}
diff --git a/src/spek-fft.h b/src/spek-fft.h
index e6f8ecb..c3ef247 100644
--- a/src/spek-fft.h
+++ b/src/spek-fft.h
@@ -1,6 +1,6 @@
 /* spek-fft.h
  *
- * Copyright (C) 2010-2012  Alexander Kojevnikov <alexander@kojevnikov.com>
+ * Copyright (C) 2010-2013  Alexander Kojevnikov <alexander@kojevnikov.com>
  *
  * Spek is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
@@ -19,26 +19,44 @@
 #ifndef SPEK_FFT_H_
 #define SPEK_FFT_H_
 
-struct RDFTContext;
+#include <memory>
+#include <vector>
 
-struct spek_fft_plan
-{
-    // Internal data.
-    struct RDFTContext *cx;
-    int n;
+class FFTPlan;
 
-    // Exposed properties.
-    float *input;
-    float *output;
+class FFT
+{
+public:
+    FFT() {}
+    std::unique_ptr<FFTPlan> create(int nbits);
 };
 
-// Allocate buffers and create a new FFT plan.
-struct spek_fft_plan * spek_fft_plan_new(int n);
-
-// Execute the FFT on plan->input.
-void spek_fft_execute(struct spek_fft_plan *p);
-
-// Destroy the plan and de-allocate buffers.
-void spek_fft_delete(struct spek_fft_plan *p);
+class FFTPlan
+{
+public:
+    FFTPlan(int nbits) :
+        nbits(nbits), input_size(1 << nbits), output_size((1 << (nbits - 1)) + 1),
+        input(input_size), output(output_size) {}
+    virtual ~FFTPlan() {}
+
+    int get_input_size() const { return this->input_size; }
+    int get_output_size() const { return this->output_size; }
+    float get_input(int i) const { return this->input[i]; }
+    void set_input(int i, float v) { this->input[i] = v; }
+    float get_output(int i) const { return this->output[i]; }
+    void set_output(int i, float v) { this->output[i] = v; }
+
+    virtual void execute() = 0;
+
+protected:
+    float *get_input() { return this->input.data(); }
+
+private:
+    int nbits;
+    int input_size;
+    int output_size;
+    std::vector<float> input;
+    std::vector<float> output;
+};
 
 #endif
diff --git a/src/spek-pipeline.cc b/src/spek-pipeline.cc
index cc194ce..493071c 100644
--- a/src/spek-pipeline.cc
+++ b/src/spek-pipeline.cc
@@ -49,12 +49,11 @@ enum
 struct spek_pipeline
 {
     std::unique_ptr<AudioFile> file;
-    int bands;
+    std::unique_ptr<FFTPlan> fft;
     int samples;
     spek_pipeline_cb cb;
     void *cb_data;
 
-    struct spek_fft_plan *fft;
     float *coss; // Pre-computed cos table.
     int nfft; // Size of the FFT transform.
     int input_size;
@@ -84,17 +83,21 @@ static void * worker_func(void *);
 static void reader_sync(struct spek_pipeline *p, int pos);
 
 struct spek_pipeline * spek_pipeline_open(
-    std::unique_ptr<AudioFile> file, int bands, int samples, spek_pipeline_cb cb, void *cb_data)
+    std::unique_ptr<AudioFile> file,
+    std::unique_ptr<FFTPlan> fft,
+    int samples,
+    spek_pipeline_cb cb,
+    void *cb_data
+)
 {
     spek_pipeline *p = new spek_pipeline();
     p->file = std::move(file);
-    p->bands = bands;
+    p->fft = std::move(fft);
     p->samples = samples;
     p->cb = cb;
     p->cb_data = cb_data;
 
     p->coss = NULL;
-    p->fft = NULL;
     p->input = NULL;
     p->output = NULL;
     p->has_reader_thread = false;
@@ -105,16 +108,15 @@ struct spek_pipeline * spek_pipeline_open(
     p->has_worker_cond = false;
 
     if (!p->file->get_error()) {
-        p->nfft = 2 * bands - 2;
+        p->nfft = p->fft->get_input_size();
         p->coss = (float*)malloc(p->nfft * sizeof(float));
         float cf = 2.0f * (float)M_PI / p->nfft;
         for (int i = 0; i < p->nfft; ++i) {
             p->coss[i] = cosf(cf * i);
         }
-        p->fft = spek_fft_plan_new(p->nfft);
         p->input_size = p->nfft * (NFFT * 2 + 1);
         p->input = (float*)malloc(p->input_size * sizeof(float));
-        p->output = (float*)malloc(bands * sizeof(float));
+        p->output = (float*)malloc(p->fft->get_output_size() * sizeof(float));
         p->file->start(samples);
     }
 
@@ -173,10 +175,6 @@ void spek_pipeline_close(struct spek_pipeline *p)
         free(p->input);
         p->input = NULL;
     }
-    if (p->fft) {
-        spek_fft_delete(p->fft);
-        p->fft = NULL;
-    }
     if (p->coss) {
         free(p->coss);
         p->coss = NULL;
@@ -357,7 +355,7 @@ static void * worker_func(void *pp)
     int head = 0, tail = 0;
     int prev_head = 0;
 
-    memset(p->output, 0, sizeof(float) * p->bands);
+    memset(p->output, 0, sizeof(float) * p->fft->get_output_size());
 
     while (true) {
         pthread_mutex_lock(&p->reader_mutex);
@@ -402,12 +400,12 @@ static void * worker_func(void *pp)
                     // val *= 0.53836f - 0.46164f * coss[i];
                     // Hann window.
                     val *= 0.5f * (1.0f - p->coss[i]);
-                    p->fft->input[i] = val;
+                    p->fft->set_input(i, val);
                 }
-                spek_fft_execute(p->fft);
+                p->fft->execute();
                 num_fft++;
-                for (int i = 0; i < p->bands; i++) {
-                    p->output[i] += p->fft->output[i];
+                for (int i = 0; i < p->fft->get_output_size(); i++) {
+                    p->output[i] += p->fft->get_output(i);
                 }
             }
 
@@ -419,14 +417,14 @@ static void * worker_func(void *pp)
                     acc_error += p->file->get_error_per_interval();
                 }
 
-                for (int i = 0; i < p->bands; i++) {
+                for (int i = 0; i < p->fft->get_output_size(); i++) {
                     p->output[i] /= num_fft;
                 }
 
                 if (sample == p->samples) break;
                 p->cb(sample++, p->output, p->cb_data);
 
-                memset(p->output, 0, sizeof(float) * p->bands);
+                memset(p->output, 0, sizeof(float) * p->fft->get_output_size());
                 frames = 0;
                 num_fft = 0;
             }
diff --git a/src/spek-pipeline.h b/src/spek-pipeline.h
index 96dec79..fa2711e 100644
--- a/src/spek-pipeline.h
+++ b/src/spek-pipeline.h
@@ -22,13 +22,18 @@
 #include <memory>
 #include <string>
 
-class Audio;
+class AudioFile;
+class FFTPlan;
 struct spek_pipeline;
 
 typedef void (*spek_pipeline_cb)(int sample, float *values, void *cb_data);
 
 struct spek_pipeline * spek_pipeline_open(
-    std::unique_ptr<AudioFile> file, int bands, int samples, spek_pipeline_cb cb, void *cb_data
+    std::unique_ptr<AudioFile> file,
+    std::unique_ptr<FFTPlan> fft,
+    int samples,
+    spek_pipeline_cb cb,
+    void *cb_data
 );
 
 void spek_pipeline_start(struct spek_pipeline *pipeline);
diff --git a/src/spek-spectrogram.cc b/src/spek-spectrogram.cc
index b799eb3..16b38a1 100644
--- a/src/spek-spectrogram.cc
+++ b/src/spek-spectrogram.cc
@@ -22,6 +22,7 @@
 
 #include "spek-audio.h"
 #include "spek-events.h"
+#include "spek-fft.h"
 #include "spek-palette.h"
 #include "spek-pipeline.h"
 #include "spek-platform.h"
@@ -43,8 +44,8 @@ enum
     MIN_RANGE = -140,
     URANGE = -20,
     LRANGE = -120,
-    NFFT = 2048,
-    BANDS = NFFT / 2 + 1,
+    FFT_BITS = 11,
+    BANDS = (1 << (FFT_BITS - 1)) + 1,
     LPAD = 60,
     TPAD = 60,
     RPAD = 90,
@@ -62,6 +63,7 @@ SpekSpectrogram::SpekSpectrogram(wxFrame *parent) :
         wxFULL_REPAINT_ON_RESIZE | wxWANTS_CHARS
     ),
     audio(new Audio()), // TODO: refactor
+    fft(new FFT()),
     pipeline(NULL),
     duration(0.0),
     sample_rate(0),
@@ -365,7 +367,7 @@ void SpekSpectrogram::start()
         this->image.Create(samples, BANDS);
         this->pipeline = spek_pipeline_open(
             this->audio->open(std::string(this->path.utf8_str())),
-            BANDS,
+            this->fft->create(FFT_BITS),
             samples,
             pipeline_cb,
             this
diff --git a/src/spek-spectrogram.h b/src/spek-spectrogram.h
index 0591672..cf0a37f 100644
--- a/src/spek-spectrogram.h
+++ b/src/spek-spectrogram.h
@@ -24,6 +24,7 @@
 #include <wx/wx.h>
 
 class Audio;
+class FFT;
 class SpekHaveSampleEvent;
 struct spek_pipeline;
 
@@ -46,6 +47,7 @@ private:
     void stop();
 
     std::unique_ptr<Audio> audio;
+    std::unique_ptr<FFT> fft;
     spek_pipeline *pipeline;
     wxString path;
     wxString desc;
